<?php 
    $date = new DateTime($_POST['date']);
    $summn = $_POST['summn'];
    $add = $_POST['add'];
    $period = $_POST['period'];
    $summadd = $add == 1 ? $_POST['summadd'] : 0;
    
    function month_count_days($date) {
        $dt = clone $date;
        
        //set first day of the month
        $dt->modify("-".((int) $dt->format("d")-1)." day");
        $dt->modify('+1 month');
        
        //count days of month
        $dt->modify("-1 day");
        $count = (int) $dt->format("d");
        
        return $count;
    }
    
    function year_count_days($date) {
        
        $year = (int) $date->format("Y");
        
        if ($year % 4 == 0) {
            $count = 366;
        } else {
            $count = 365;
        }
        return $count;
    }
    
    $PERCENT = 0.1;
    //set first day of month
    $date->modify("-".((int)$date->format("d") - 1)." day");
    $dateEnd = clone $date;
    $dateEnd->modify("+".$period." year");
    //calculate
    while ($date->format("Ym") != $dateEnd->format("Ym")) {
        
        $daysn = month_count_days($date);
        //count days in year
        $daysy = year_count_days($date);   
        
        $summn = $summn + ($summn + $summadd) * $daysn * $PERCENT / $daysy;
        
        $date->modify("+1 month");
        
    }
    
    echo round ($summn,2);
?>
