$(function() {
    console.log('ready');

    /*datepicker widjet*/
     
    
    $( "#dt" ).datepicker({
        dateFormat: "dd.mm.yy"
    });

    /**
     * multipler summ
     * @param {*number} v 
     */
    function multipler(v) {
        function multipler_val(v) {
            if (v > 97) return (v - 97) * 1000000;
            if (v > 80) return v * 10000;
            if (v > 50) return v * 10000;
            if (v <= 50) return v * 1000;
        }
        return multipler_val(v).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
    
    //clear validation
    function clear() {
        $("input").each(function() {
            $(this).find("+ span").remove();
            $(this).removeClass("error");
        });
    }
   
    /**
     * add error
     * @param text{*string} text error
     */
    $.fn.addError = function(text) {
        this.addClass("error");
        this.after('<span>'+text+'</span>');
    };
    
    //default values
    $('#summ, #summ2').each(function() {
        this.value = multipler($("[target=" + this.id + "]")[0].value);
    });

    $("#fader, #fader1").mousedown(function() {
        var s = $('#' + $(this).attr("target"))[0];
        s.value = multipler(this.value);
        
        $(this).mousemove(function() {
            s.value = multipler(this.value);
        });

    }).mouseup(function() {
        $(this).unbind('mousemove');
    }).mouseout(function() {
        $(this).unbind('mousemove');
    });

    $('#menu li>a').click(function() {
        if (!$(this).hasClass('active')) {
            $('#menu li>a').removeClass('active');
            $(this).addClass('active');

            if ($('.breadcrumbs li').length > 2)
                $('.breadcrumbs li:nth-child(n+3)').remove();
            $('.breadcrumbs li>a')[1].innerHTML = $(this).text();
        }
    });
    
    //Period
    $('#period').change(function() {
        console.log(this.value);
    });
    
    //sum
    $('#summ, #summ2').on("keyup", function() {
        this.value = this.value.replace(/ /g,'');
        var number = this.value;
        this.value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    });
    
    //result
    $("#commit").click(function() {
        $("#result")[0].innerHTML = "";
        
        //clear validation
        clear();
        
        //validate
        
        $('#summ, #summ2').each(function() {
            var val = this.value.replace(/ /g,'');
            
            if (isNaN(val)) {
                $(this).addError("сумма должны быть числом");
            } else if (val < 1000) {
                $(this).addError("сумма должна быть > 1 000");
            } else if (val > 3000000) {
                $(this).addError("сумма должна быть < 3 000 000");
            }
        });
        
        var d = $("#dt")[0].value;
        if (/^\d{2}\.\d{2}\.\d{4}$/i.test(d) != true) {
            $("#dt").addError('неверный формат даты');
        }
        
        
        if ($(".error").length == 0) {
            $.ajax({url: "php/calc.php",
                    method: "POST",
                    data: { 
                        date: $("#dt")[0].value,
                        summn: $("#summ")[0].value.replace(/ /g,''),
                        add: $('[type="radio"]')[1].checked ? 1 : 0,
                        period: $("#period")[0].value,
                        summadd: $("#summ2")[0].value.replace(/ /g,'')
                    }
            })
            
            .done(function (data) {
                $("#result")[0].innerHTML = "<label>Результат: </label>" + "<span>"+ data.replace(/\B(?=(\d{3})+(?!\d))/g, " ") +"</span>";
            });
        }
    });
});